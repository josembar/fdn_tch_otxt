with Detalle ([DataID],[Name],[Level],[SubType])
			as
			(
				select 
					-- p.ParentID
					(case p.SubType   
					when 31066 then -p.DataID
					when 848 then -p.DataID
					else p.DataID
					end) as DataID,
					p.Name,
					0 as [Level],
					p.SubType
					from 
					[csadmin].DTree p
				where 
					p.DataID = 92326 --Starting Folder parametro a ingresar
					AND p.Deleted = 0

				union all

				select
					-- c.ParentID
					c.DataID,
					c.Name,
					[Level] + 1,
					c.SubType
				from
					[csadmin].DTree c
					inner join Detalle d ON 
						d.DataID = c.ParentID --abs(c.ParentID)Algunos registros son negativos
						and c.Deleted = 0
			)

			select
				Detalle.*,
				Permisos.RightID 'ID del Usuario/Grupo', 
				Personas.Name 'Nombre del Usuario/Grupo', 
				Permisos.Permissions,
				Personas.Type 'Usuario/Grupo'
				from
				Detalle, csadmin.DTreeACL Permisos,csadmin.KUAF Personas
				where SubType in (848,31066) -- (zona de negocio,archivador)
				and Deleted = 0
				and Permisos.RightID = Personas.ID
				and Personas.Type in(0,1) --cero es usuario, uno es grupo
				and Detalle.DataID = Permisos.DataID