/*
Carpetas sobre las cuales se debe mostrar la matriz de permisos.
Es decir, la tabla de permisos debe aparecer para los elementos
que están dentro de esas carpetas, o una carpeta de esas.
Debe mostrar los permisos que tiene un usuario, un grupo o todos
los anteriores
*/

select DataID,Name,SubType from DTree 
where DataID in(552310,92332,938984,764250,92319,
92321,950877,92322,92324,92326,92323,976300,
939857,564474,939043,952521,884405,892947,
930795)
-- 552310 Clasificados FDN subtypo 207, no incluir

/*
Campo catalog en la DTree
El campo Catalog toma los siguientes valores: 
0 quiere decir que no se muestra en la parte de arriba

*/
select count(*) from DTree 
where Catalog = 1 and ParentID = 2000


--Puede servir
SELECT Padre.DataID 'ID papá',Padre.Name 'Papá',
Hijo.DataID 'ID hijo',Hijo.Name 'Hijo'
FROM DTree Padre
inner join DTree Hijo
on Padre.DataID = Hijo.ParentID
WHERE Padre.ParentID = %1 AND Hijo.Deleted = 0

-- LR Reporte de permisos por objeto
select Hijo.ParentID,
Hijo.DataID 'ID hijo',Hijo.Name 'Hijo',
Permisos.RightID 'ID del Usuario/Grupo', 
Personas.Name 'Nombre del Usuario/Grupo', 
Permisos.Permissions, Personas.Type 'Usuario/Grupo'
from DTree Hijo,DTreeACL Permisos, KUAF Personas
where Hijo.Deleted = 0
and Permisos.RightID = Personas.ID
and Hijo.SubType = 0
and Personas.Type in(0,1) --cero es usuario, uno es grupo
and Hijo.ParentID = 938984 --parametro a ingresar
and Hijo.DataID = Permisos.DataID
order by Hijo.Name

-- 938984

--búsqueda por nombre de carpeta

select Padre.DataID 'Id papá',
Padre.Name 'Papá',
Hijo.DataID 'ID hijo',
Hijo.Name 'Hijo',
Permisos.RightID 'ID del Usuario/Grupo', 
Personas.Name 'Nombre del Usuario/Grupo', 
Permisos.Permissions,
Personas.Type 'Usuario/Grupo'
from DTree Hijo,DTree Padre,DTreeACL Permisos, KUAF Personas
where Hijo.ParentID = Padre.DataID
and Hijo.Deleted = 0
and Permisos.RightID = Personas.ID
and Hijo.SubType = 0
and Personas.Type in(0,1) --cero es usuario, uno es grupo
~2 --parametro a ingresar
and Hijo.DataID = Permisos.DataID
order by Hijo.Name

--01 Análisis de Coyuntura Macro-Financiera >> Katherine Rojas Alba (krojas) &  G_INT_ECM_INVESTIGACIONES_EDICION

-- and Hijo.ParentID = %1

--Content Server Document Templates:Reportes:01 General:05 Reporte Administración Workflows:Ayudas:WR_estados

/*
Consulta parametrizada para que me muestre las carpetas que están "arriba" junto con sus hijos
y los respectivos permisos
*/

select Padre.DataID 'Id papá',
Padre.Name 'Papá',
Hijo.DataID 'ID hijo',
Hijo.Name 'Hijo',
Permisos.RightID 'ID del Usuario/Grupo', 
Personas.Name 'Nombre del Usuario/Grupo', 
Permisos.Permissions,
Personas.Type 'Usuario/Grupo'
from DTree Hijo,DTree Padre,DTreeACL Permisos, KUAF Personas
where Hijo.ParentID = Padre.DataID
and Hijo.Deleted = 0
and Permisos.RightID = Personas.ID
and Hijo.SubType = 0
and Personas.Type in(0,1) --cero es usuario, uno es grupo
~2 --parametro a ingresar --> and Padre.Name = %1
and Hijo.DataID = Permisos.DataID
and Padre.Catalog = 1 
and Padre.ParentID = 2000
and Padre.Deleted = 0 
and Padre.SubType = 0
order by Padre.Name,Hijo.Name